// P3.h: interface for the CP3 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_H__135DE93A_AA33_41C8_8A09_051F8C0C1A1B__INCLUDED_)
#define AFX_P3_H__135DE93A_AA33_41C8_8A09_051F8C0C1A1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CP3
{
public:
	CP3(void);
	~CP3(void);
	CP3(double x,double y,double z);

	friend CP3 operator*(double scale,const CP3&P);//���������
	friend CP3 operator*(const CP3&P,double scale);//���������
	friend CP3 operator+(const CP3&P1,const CP3&P2);
public:
	double x;
	double y;
	double z;
	double w;
};

#endif // !defined(AFX_P3_H__135DE93A_AA33_41C8_8A09_051F8C0C1A1B__INCLUDED_)
