// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here

}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

void CTestView::ReadPoint(void)
{
	//绘制曲面的控制点
	P3[0][0].x=20,P3[0][0].y=0,P3[0][0].z=200;
	P3[0][1].x=0,P3[0][1].y=100,P3[0][1].z=150;
	P3[0][2].x=-130,P3[0][2].y=100,P3[0][2].z=50;
	P3[0][3].x=-250,P3[0][3].y=50,P3[0][3].z=0;

	P3[1][0].x=100,P3[1][0].y=100,P3[1][0].z=150;
	P3[1][1].x=30,P3[1][1].y=100,P3[1][1].z=100;
	P3[1][2].x=-40,P3[1][2].y=100,P3[1][2].z=50;
	P3[1][3].x=-110,P3[1][3].y=100,P3[1][3].z=0;

	P3[2][0].x=280,P3[2][0].y=90,P3[2][0].z=140;
	P3[2][1].x=110,P3[2][1].y=120,P3[2][1].z=80;
	P3[2][2].x=0,P3[2][2].y=130,P3[2][2].z=30;
	P3[2][3].x=-100,P3[2][3].y=150,P3[2][3].z=-50;

	P3[3][0].x=350,P3[3][0].y=30,P3[3][0].z=150;
	P3[3][1].x=200,P3[3][1].y=150,P3[3][1].z=50;
	P3[3][2].x=50,P3[3][2].y=200,P3[3][2].z=0;
	P3[3][3].x=0,P3[3][3].y=100,P3[3][3].z=-70;

}
void CTestView::ReadVertex(void)
{
	double r=200;//球的半径
	const double m=0.5523;//球半径计算的魔术常数
	//第一卦限控制点
	Vertex[0].x=0.0, Vertex[0].y=r, Vertex[0].z=0.0;
	Vertex[1].x=0.0, Vertex[1].y=r, Vertex[1].z=m*r;
	Vertex[2].x=0.0, Vertex[2].y=m*r, Vertex[2].z=r;
	Vertex[3].x=0.0, Vertex[3].y=0.0, Vertex[3].z=r;
	Vertex[4].x=m*m*r, Vertex[4].y=r, Vertex[4].z=m*r;
	Vertex[5].x=m*r, Vertex[5].y=m*r, Vertex[5].z=r;
	Vertex[6].x=m*r, Vertex[6].y=0, Vertex[6].z=r;
	Vertex[7].x=m*r, Vertex[7].y=r, Vertex[7].z=m*m*r;
	Vertex[8].x=r, Vertex[8].y=m*r, Vertex[8].z=m*r;
	Vertex[9].x=r, Vertex[9].y=0.0, Vertex[9].z=m*r;
	Vertex[10].x=m*r, Vertex[10].y=r, Vertex[10].z=0.0;
	Vertex[11].x=r, Vertex[11].y=m*r, Vertex[11].z=0.0;
	Vertex[12].x=r, Vertex[12].y=0.0, Vertex[12].z=0.0;

	//第二卦限控制点
	Vertex[13].x=m*r, Vertex[13].y=r, Vertex[13].z=-m*m*r;
	Vertex[14].x=r, Vertex[14].y=m*r, Vertex[14].z=-m*r;
	Vertex[15].x=r, Vertex[15].y=0.0, Vertex[15].z=-m*r;
	Vertex[16].x=m*m*r, Vertex[16].y=r, Vertex[16].z=-m*r;
	Vertex[17].x=m*r, Vertex[17].y=m*r, Vertex[17].z=-r;
	Vertex[18].x=m*r, Vertex[18].y=0.0, Vertex[18].z=-r;
	Vertex[19].x=0.0, Vertex[19].y=r, Vertex[19].z=-m*r;
	Vertex[20].x=0.0, Vertex[20].y=m*r, Vertex[20].z=-r;
	Vertex[21].x=0.0, Vertex[21].y=0.0, Vertex[21].z=-r;

	//第三卦限控制点
	Vertex[22].x=-m*m*r, Vertex[22].y=r, Vertex[22].z=-m*r;
	Vertex[23].x=-m*r, Vertex[23].y=m*r, Vertex[23].z=-r;
	Vertex[24].x=-m*r, Vertex[24].y=0.0, Vertex[24].z=-r;
	Vertex[25].x=-m*r, Vertex[25].y=r, Vertex[25].z=-m*m*r;
	Vertex[26].x=-r, Vertex[26].y=m*r, Vertex[26].z=-m*r;
	Vertex[27].x=-r, Vertex[27].y=0.0, Vertex[27].z=-m*r;
	Vertex[28].x=-m*r, Vertex[28].y=r, Vertex[28].z=0.0;
	Vertex[29].x=-r, Vertex[29].y=m*r, Vertex[29].z=0.0;
	Vertex[30].x=-r, Vertex[30].y=0.0, Vertex[30].z=0.0;

	//第四卦限控制点
	Vertex[31].x=-m*r, Vertex[31].y=r, Vertex[31].z=m*m*r;
	Vertex[32].x=-r, Vertex[32].y=m*r, Vertex[32].z=m*r;
	Vertex[33].x=-r, Vertex[33].y=0.0, Vertex[33].z=m*r;
	Vertex[34].x=-m*m*r, Vertex[34].y=r, Vertex[34].z=m*r;
	Vertex[35].x=-m*r, Vertex[35].y=m*r, Vertex[35].z=r;
	Vertex[36].x=-m*r, Vertex[36].y=0.0, Vertex[36].z=r;

	//第五卦限控制点
	Vertex[37].x=0.0, Vertex[37].y=-m*r, Vertex[37].z=r;
	Vertex[38].x=0.0, Vertex[38].y=-r, Vertex[38].z=m*r;
	Vertex[39].x=m*r, Vertex[39].y=-m*r, Vertex[39].z=r;
	Vertex[40].x=m*m*r, Vertex[40].y=-r, Vertex[40].z=m*r;
	Vertex[41].x=r, Vertex[41].y=-m*r, Vertex[41].z=m*r;
	Vertex[42].x=m*r, Vertex[42].y=-r, Vertex[42].z=m*m*r;
	Vertex[43].x=r, Vertex[43].y=-m*r, Vertex[43].z=0.0;
	Vertex[44].x=m*r, Vertex[44].y=-r, Vertex[44].z=0.0;

	//第六卦限控制点
	Vertex[45].x=r, Vertex[45].y=-m*r, Vertex[45].z=-m*r;
	Vertex[46].x=m*r, Vertex[46].y=-r, Vertex[46].z=-m*m*r;
	Vertex[47].x=m*r, Vertex[47].y=-m*r, Vertex[47].z=-r;
	Vertex[48].x=m*m*r, Vertex[48].y=-r, Vertex[48].z=-m*r;
	Vertex[49].x=0.0, Vertex[49].y=-m*r, Vertex[49].z=-r;
	Vertex[50].x=0.0, Vertex[50].y=-r, Vertex[50].z=-m*r;

	//第七卦限控制点
	Vertex[51].x=-m*r, Vertex[51].y=-m*r, Vertex[51].z=-r;
	Vertex[52].x=-m*m*r, Vertex[52].y=-r, Vertex[52].z=-m*r;
	Vertex[53].x=-r, Vertex[53].y=-m*r, Vertex[53].z=-m*r;
	Vertex[54].x=-m*r, Vertex[54].y=-r, Vertex[54].z=-m*m*r;
	Vertex[55].x=-r, Vertex[55].y=-m*r, Vertex[55].z=0.0;
	Vertex[56].x=-m*r, Vertex[56].y=-r, Vertex[56].z=0.0;

	//第八卦限控制点
	Vertex[57].x=-r, Vertex[57].y=-m*r, Vertex[57].z=m*r;
	Vertex[58].x=-m*r, Vertex[58].y=-r, Vertex[58].z=m*m*r;
	Vertex[59].x=-m*r, Vertex[59].y=-m*r, Vertex[59].z=r;
	Vertex[60].x=-m*m*r, Vertex[60].y=-r, Vertex[60].z=m*r;
	Vertex[61].x=0.0, Vertex[61].y=-r, Vertex[61].z=0.0;
}


void CTestView::ReadPatch(void)
{
	//第一卦限的曲面片
	S[0].pNumber=16;//曲面片上的控制点数控制点数
	S[0].pIndex[0][0]=0, S[0].pIndex[0][1]=1, S[0].pIndex[0][2]=2, S[0].pIndex[0][3]=3;
	S[0].pIndex[1][0]=0, S[0].pIndex[1][1]=4, S[0].pIndex[1][2]=5, S[0].pIndex[1][3]=6;
	S[0].pIndex[2][0]=0, S[0].pIndex[2][1]=7, S[0].pIndex[2][2]=8, S[0].pIndex[2][3]=9;
	S[0].pIndex[3][0]=0, S[0].pIndex[3][1]=10, S[0].pIndex[3][2]=11, S[0].pIndex[3][3]=12;

	//第二卦限的曲面片
	S[1].pNumber=16;//曲面片上的控制点数控制点数
	S[1].pIndex[0][0]=0, S[1].pIndex[0][1]=10, S[1].pIndex[0][2]=11, S[1].pIndex[0][3]=12;
	S[1].pIndex[1][0]=0, S[1].pIndex[1][1]=13, S[1].pIndex[1][2]=14, S[1].pIndex[1][3]=15;
	S[1].pIndex[2][0]=0, S[1].pIndex[2][1]=16, S[1].pIndex[2][2]=17, S[1].pIndex[2][3]=18;
	S[1].pIndex[3][0]=0, S[1].pIndex[3][1]=19, S[1].pIndex[3][2]=20, S[1].pIndex[3][3]=21;

	//第三卦限的曲面片
	S[2].pNumber=16;//曲面片上的控制点数控制点数
	S[2].pIndex[0][0]=0, S[2].pIndex[0][1]=19, S[2].pIndex[0][2]=20, S[2].pIndex[0][3]=21;
	S[2].pIndex[1][0]=0, S[2].pIndex[1][1]=22, S[2].pIndex[1][2]=23, S[2].pIndex[1][3]=24;
	S[2].pIndex[2][0]=0, S[2].pIndex[2][1]=25, S[2].pIndex[2][2]=26, S[2].pIndex[2][3]=27;
	S[2].pIndex[3][0]=0, S[2].pIndex[3][1]=28, S[2].pIndex[3][2]=29, S[2].pIndex[3][3]=30;

	//第四卦限的曲面片
	S[3].pNumber=16;//曲面片上的控制点数控制点数
	S[3].pIndex[0][0]=0, S[3].pIndex[0][1]=28, S[3].pIndex[0][2]=29, S[3].pIndex[0][3]=30;
	S[3].pIndex[1][0]=0, S[3].pIndex[1][1]=31, S[3].pIndex[1][2]=32, S[3].pIndex[1][3]=33;
	S[3].pIndex[2][0]=0, S[3].pIndex[2][1]=34, S[3].pIndex[2][2]=35, S[3].pIndex[2][3]=36;
	S[3].pIndex[3][0]=0, S[3].pIndex[3][1]=1, S[3].pIndex[3][2]=2, S[3].pIndex[3][3]=3;

	//第五卦限的曲面片
	S[4].pNumber=16;//曲面片上的控制点数控制点数
	S[4].pIndex[0][0]=3, S[4].pIndex[0][1]=37, S[4].pIndex[0][2]=38, S[4].pIndex[0][3]=61;
	S[4].pIndex[1][0]=6, S[4].pIndex[1][1]=39, S[4].pIndex[1][2]=40, S[4].pIndex[1][3]=61;
	S[4].pIndex[2][0]=9, S[4].pIndex[2][1]=41, S[4].pIndex[2][2]=42, S[4].pIndex[2][3]=61;
	S[4].pIndex[3][0]=12, S[4].pIndex[3][1]=43, S[4].pIndex[3][2]=44, S[4].pIndex[3][3]=61;

	//第六卦限的曲面片
	S[5].pNumber=16;//曲面片上的控制点数控制点数
	S[5].pIndex[0][0]=12, S[5].pIndex[0][1]=43, S[5].pIndex[0][2]=44, S[5].pIndex[0][3]=61;
	S[5].pIndex[1][0]=15, S[5].pIndex[1][1]=45, S[5].pIndex[1][2]=46, S[5].pIndex[1][3]=61;
	S[5].pIndex[2][0]=18, S[5].pIndex[2][1]=47, S[5].pIndex[2][2]=48, S[5].pIndex[2][3]=61;
	S[5].pIndex[3][0]=21, S[5].pIndex[3][1]=49, S[5].pIndex[3][2]=50, S[5].pIndex[3][3]=61;

	//第七卦限的曲面片
	S[6].pNumber=16;//曲面片上的控制点数控制点数
	S[6].pIndex[0][0]=21, S[6].pIndex[0][1]=49, S[6].pIndex[0][2]=50, S[6].pIndex[0][3]=61;
	S[6].pIndex[1][0]=24, S[6].pIndex[1][1]=51, S[6].pIndex[1][2]=52, S[6].pIndex[1][3]=61;
	S[6].pIndex[2][0]=27, S[6].pIndex[2][1]=53, S[6].pIndex[2][2]=54, S[6].pIndex[2][3]=61;
	S[6].pIndex[3][0]=30, S[6].pIndex[3][1]=55, S[6].pIndex[3][2]=56, S[6].pIndex[3][3]=61;

	//第八卦限的曲面片
	S[7].pNumber=16;//曲面片上的控制点数控制点数
	S[7].pIndex[0][0]=30, S[7].pIndex[0][1]=55, S[7].pIndex[0][2]=56, S[7].pIndex[0][3]=61;
	S[7].pIndex[1][0]=33, S[7].pIndex[1][1]=57, S[7].pIndex[1][2]=58, S[7].pIndex[1][3]=61;
	S[7].pIndex[2][0]=36, S[7].pIndex[2][1]=59, S[7].pIndex[2][2]=60, S[7].pIndex[2][3]=61;
	S[7].pIndex[3][0]=3, S[7].pIndex[3][1]=37, S[7].pIndex[3][2]=38, S[7].pIndex[3][3]=61;
}


void CTestView::DrawBezierSphere(CDC*pDC)
{
	for(int nPatch=0;nPatch<8;nPatch++)
	{
		for(int i=0;i<4;i++)
		{
			for(int j=0;j<4;j++)
			{
				P3[i][j]=Vertex[S[nPatch].pIndex[i][j]];
			}
		}
		patch.ReadControlPoint(P3);
		patch.DrawControlGrid(pDC);
		patch.DrawCurvedPatch(pDC);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CRect rect;//定义客户区矩形
	GetClientRect(&rect);//获得客户区矩形的信息
	pDC->SetMapMode(MM_ANISOTROPIC);//自定义二维坐标系
	pDC->SetWindowExt(rect.Width(), rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(), -rect.Height());//设置视区范围，且x轴水平向右为正，y轴垂直向上为正
	pDC->SetViewportOrg(rect.Width() / 2, rect.Height() / 2);//设置客户区中心为二维坐标系原点
	rect.OffsetRect(-rect.Width() / 2, -rect.Height() / 2);//rect矩形与客户区重合

	ReadPoint();
	teapot.DrawTeapot(pDC);
}
	// TODO: add draw code for native data here

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers
