// Patch.h: interface for the CPatch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PATCH_H__4C138CE9_A857_4CED_A4AD_55FD444BCEB8__INCLUDED_)
#define AFX_PATCH_H__4C138CE9_A857_4CED_A4AD_55FD444BCEB8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPatch
{
public:
	CPatch(void);
	~CPatch(void);
public:
	int pNumber;//表面顶点数
	int pIndex[4][4];//表面顶点索引号
};

#endif // !defined(AFX_PATCH_H__4C138CE9_A857_4CED_A4AD_55FD444BCEB8__INCLUDED_)
