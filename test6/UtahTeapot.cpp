// UtahTeapot.cpp: implementation of the UtahTeapot class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Test.h"
#include "UtahTeapot.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

UtahTeapot::UtahTeapot(void)
{
	ReadPoint();
	ReadHandleControlPoint();
	ReadMouseControlPoint();
}


UtahTeapot::~UtahTeapot(void)
{
}


void UtahTeapot::ReadPoint(void)
{
	//��ֵ������
	bodyPoint[0][0].x=1.4*100, bodyPoint[0][0].y=2.25*100, bodyPoint[0][0].z=0;
	bodyPoint[0][1].x=1.3375*100, bodyPoint[0][1].y=2.38125*100, bodyPoint[0][1].z=0;
	bodyPoint[0][2].x=1.4375*100, bodyPoint[0][2].y=2.38125*100, bodyPoint[0][2].z=0;
	bodyPoint[0][3].x=1.5*100, bodyPoint[0][3].y=2.25*100, bodyPoint[0][3].z=0;

	bodyPoint[1][0].x=1.5*100, bodyPoint[1][0].y=2.25*100, bodyPoint[1][0].z=0;
	bodyPoint[1][1].x=1.75*100, bodyPoint[1][1].y=1.725*100, bodyPoint[1][1].z=0;
	bodyPoint[1][2].x=2.0*100, bodyPoint[1][2].y=1.2*100, bodyPoint[1][2].z=0;
	bodyPoint[1][3].x=2.0*100, bodyPoint[1][3].y=0.75*100, bodyPoint[1][3].z=0;

	bodyPoint[2][0].x=2.0*100, bodyPoint[2][0].y=0.75*100, bodyPoint[2][0].z=0;
	bodyPoint[2][1].x=2.0*100, bodyPoint[2][1].y=0.3*100, bodyPoint[2][1].z=0;
	bodyPoint[2][2].x=1.5*100, bodyPoint[2][2].y=0.075*100, bodyPoint[2][2].z=0;
	bodyPoint[2][3].x=1.5*100, bodyPoint[2][3].y=0.0*100, bodyPoint[2][3].z=0;

	//��ֵ������
	gaiPoint[0][0].x=0.0*100 ,gaiPoint[0][0].y=3.0*100, gaiPoint[0][0].z=0.0;
	gaiPoint[0][1].x=0.8*100 ,gaiPoint[0][1].y=3.0*100, gaiPoint[0][1].z=0.0;
	gaiPoint[0][2].x=0.0 ,gaiPoint[0][2].y=2.70*100, gaiPoint[0][2].z=0.0;
	gaiPoint[0][3].x=0.2*100 ,gaiPoint[0][3].y=2.55*100, gaiPoint[0][3].z=0.0;

	gaiPoint[1][0].x=0.2*100 ,gaiPoint[1][0].y=2.55*100, gaiPoint[1][0].z=0.0;
	gaiPoint[1][1].x=0.4*100 ,gaiPoint[1][1].y=2.40*100, gaiPoint[1][1].z=0.0;
	gaiPoint[1][2].x=1.3*100 ,gaiPoint[1][2].y=2.40*100, gaiPoint[1][2].z=0.0;
	gaiPoint[1][3].x=1.3*100 ,gaiPoint[1][3].y=2.25*100, gaiPoint[1][3].z=0.0;

	//��ֵ��ǵײ�
	bottom[0][0].x=0.0, bottom[0][0].y=-0.15*100, bottom[0][0].z=0.0;
	bottom[0][1].x=1.425*100, bottom[0][1].y=-0.15*100, bottom[0][1].z=0.0;
	bottom[0][2].x=1.5*100, bottom[0][2].y=-0.075*100, bottom[0][2].z=0.0;
	bottom[0][3].x=1.5*100, bottom[0][3].y=0, bottom[0][3].z=0.0;

}


void UtahTeapot::DrawTeapot(CDC*pDC)
{
	//���Ʋ����
	for(int i=0;i<3;i++)
	{
		CP3 tempP[4];
		for(int j=0;j<4;j++)
		{
			tempP[j]=bodyPoint[i][j];
		}
		draw.ReadCubicBezierControlPoint(tempP);
		draw.DrawRevolutionSurface(pDC);
	}
	//���Ʊ���
	for(i=0;i<2;i++)
	{
		CP3 tempP[4];
		for(int j=0;j<4;j++)
		{
			tempP[j]=gaiPoint[i][j];
		}
		draw.ReadCubicBezierControlPoint(tempP);
		draw.DrawRevolutionSurface(pDC);
	}

	//���Ƶײ�
	for(i=0;i<1;i++)
	{
		CP3 tempP[4];
		for(int j=0;j<4;j++)
		{
			tempP[j]=bottom[i][j];
		}
		draw.ReadCubicBezierControlPoint(tempP);
		draw.DrawRevolutionSurface(pDC);
	}

	//���Ʋ������
	for(int nPatch=0;nPatch<4;nPatch++)
	{
		for(i=0;i<4;i++)
		{
			for(int j=0;j<4;j++)
			{
				switch(nPatch)
				{
				case 0:
					P3[i][j]=handle[i][j];
					continue;
				case 1:
					P3[i][j]=handle2[i][j];
					continue;
				case 2:
					P3[i][j]=handle3[i][j];
					continue;
				case 3:
					P3[i][j]=handle4[i][j];
					continue;
				}
			
			}
		}
		drawBzeierPatch.ReadControlPoint(P3);
		//drawBzeierPatch.DrawControlGrid(pDC); //���ƿ���Bezier����
		drawBzeierPatch.DrawCurvedPatch(pDC);
	}

	//���Ʋ���첿
	for(nPatch=0;nPatch<4;nPatch++)
	{
		for(int i=0;i<4;i++)
		{
			for(int j=0;j<4;j++)
			{
				switch(nPatch)
				{
				case 0:
					P3[i][j]=mouse[i][j];
					continue;
				case 1:
					P3[i][j]=mouse2[i][j];
					continue;
				case 2:
					P3[i][j]=mouse3[i][j];
					continue;
				case 3:
					P3[i][j]=mouse4[i][j];
					continue;
				}

			}
		}
		drawBzeierPatch.ReadControlPoint(P3);
		//drawBzeierPatch.DrawControlGrid(pDC);
		drawBzeierPatch.DrawCurvedPatch(pDC);
	}
}


void UtahTeapot::ReadHandleControlPoint(void)
{
	double r=0.1125;
	//��ֵ���������Ƭ1
	handle[0][0].x=-1.6*100, handle[0][0].y=1.875*100, handle[0][0].z=0.0;
	handle[0][1].x=-2.3*100, handle[0][1].y=1.875*100, handle[0][1].z=0.0;
	handle[0][2].x=-2.7*100, handle[0][2].y=1.875*100, handle[0][2].z=0.0;
	handle[0][3].x=-2.7*100, handle[0][3].y=1.65*100, handle[0][3].z=0.0;

	handle[1][0].x=-1.6*100, handle[1][0].y=1.875*100, handle[1][0].z=r;
	handle[1][1].x=-2.3*100, handle[1][1].y=1.875*100, handle[1][1].z=r;
	handle[1][2].x=-2.7*100, handle[1][2].y=1.875*100, handle[1][2].z=r;
	handle[1][3].x=-2.7*100, handle[1][3].y=1.65*100, handle[1][3].z=r;

	handle[2][0].x=-1.5*100, handle[2][0].y=2.1*100, handle[2][0].z=r;
	handle[2][1].x=-2.5*100, handle[2][1].y=2.1*100, handle[2][1].z=r;
	handle[2][2].x=-3.0*100, handle[2][2].y=2.1*100, handle[2][2].z=r;
	handle[2][3].x=-3.0*100, handle[2][3].y=1.65*100, handle[2][3].z=r;

	handle[3][0].x=-1.5*100, handle[3][0].y=2.1*100, handle[3][0].z=0.0;
	handle[3][1].x=-2.5*100, handle[3][1].y=2.1*100, handle[3][1].z=0.0;
	handle[3][2].x=-3.0*100, handle[3][2].y=2.1*100, handle[3][2].z=0.0;
	handle[3][3].x=-3.0*100, handle[3][3].y=1.65*100, handle[3][3].z=0.0;

	//��ֵ���������Ƭ2
	handle2[0][0].x=-2.7*100, handle2[0][0].y=1.65*100, handle2[0][0].z=0.0;
	handle2[0][1].x=-2.7*100, handle2[0][1].y=1.425*100, handle2[0][1].z=0.0;
	handle2[0][2].x=-2.5*100, handle2[0][2].y=0.975*100, handle2[0][2].z=0.0;
	handle2[0][3].x=-2.0*100, handle2[0][3].y=0.75*100, handle2[0][3].z=0.0;

	handle2[1][0].x=-2.7*100, handle2[1][0].y=1.65*100, handle2[1][0].z=r;
	handle2[1][1].x=-2.7*100, handle2[1][1].y=1.425*100, handle2[1][1].z=r;
	handle2[1][2].x=-2.5*100, handle2[1][2].y=0.975*100, handle2[1][2].z=r;
	handle2[1][3].x=-2.0*100, handle2[1][3].y=0.75*100, handle2[1][3].z=r;

	handle2[2][0].x=-3.0*100, handle2[2][0].y=1.65*100, handle2[2][0].z=r;
	handle2[2][1].x=-3.0*100, handle2[2][1].y=1.2*100, handle2[2][1].z=r;
	handle2[2][2].x=-2.65*100, handle2[2][2].y=0.7875*100, handle2[2][2].z=r;
	handle2[2][3].x=-1.9*100, handle2[2][3].y=0.45*100, handle2[2][3].z=r;

	handle2[3][0].x=-3.0*100, handle2[3][0].y=1.65*100, handle2[3][0].z=0.0;
	handle2[3][1].x=-3.0*100, handle2[3][1].y=1.2*100, handle2[3][1].z=0.0;
	handle2[3][2].x=-2.65*100, handle2[3][2].y=0.7875*100, handle2[3][2].z=0.0;
	handle2[3][3].x=-1.9*100, handle2[3][3].y=0.45*100, handle2[3][3].z=0.0;

	//��ֵ���������Ƭ3
	handle[0][0].x=-1.6*100, handle[0][0].y=1.875*100, handle[0][0].z=0.0;
	handle[0][1].x=-2.3*100, handle[0][1].y=1.875*100, handle[0][1].z=0.0;
	handle[0][2].x=-2.7*100, handle[0][2].y=1.875*100, handle[0][2].z=0.0;
	handle[0][3].x=-2.7*100, handle[0][3].y=1.65*100, handle[0][3].z=0.0;

	handle[1][0].x=-1.6*100, handle[1][0].y=1.875*100, handle[1][0].z=-r;
	handle[1][1].x=-2.3*100, handle[1][1].y=1.875*100, handle[1][1].z=-r;
	handle[1][2].x=-2.7*100, handle[1][2].y=1.875*100, handle[1][2].z=-r;
	handle[1][3].x=-2.7*100, handle[1][3].y=1.65*100, handle[1][3].z=-r;

	handle[2][0].x=-1.5*100, handle[2][0].y=2.1*100, handle[2][0].z=-r;
	handle[2][1].x=-2.5*100, handle[2][1].y=2.1*100, handle[2][1].z=-r;
	handle[2][2].x=-3.0*100, handle[2][2].y=2.1*100, handle[2][2].z=-r;
	handle[2][3].x=-3.0*100, handle[2][3].y=1.65*100, handle[2][3].z=-r;

	handle[3][0].x=-1.5*100, handle[3][0].y=2.1*100, handle[3][0].z=0.0;
	handle[3][1].x=-2.5*100, handle[3][1].y=2.1*100, handle[3][1].z=0.0;
	handle[3][2].x=-3.0*100, handle[3][2].y=2.1*100, handle[3][2].z=0.0;
	handle[3][3].x=-3.0*100, handle[3][3].y=1.65*100, handle[3][3].z=0.0;

	//��ֵ���������Ƭ4
	handle2[0][0].x=-2.7*100, handle2[0][0].y=1.65*100, handle2[0][0].z=0.0;
	handle2[0][1].x=-2.7*100, handle2[0][1].y=1.425*100, handle2[0][1].z=0.0;
	handle2[0][2].x=-2.5*100, handle2[0][2].y=0.975*100, handle2[0][2].z=0.0;
	handle2[0][3].x=-2.0*100, handle2[0][3].y=0.75*100, handle2[0][3].z=0.0;

	handle2[1][0].x=-2.7*100, handle2[1][0].y=1.65*100, handle2[1][0].z=-r;
	handle2[1][1].x=-2.7*100, handle2[1][1].y=1.425*100, handle2[1][1].z=-r;
	handle2[1][2].x=-2.5*100, handle2[1][2].y=0.975*100, handle2[1][2].z=-r;
	handle2[1][3].x=-2.0*100, handle2[1][3].y=0.75*100, handle2[1][3].z=-r;

	handle2[2][0].x=-3.0*100, handle2[2][0].y=1.65*100, handle2[2][0].z=-r;
	handle2[2][1].x=-3.0*100, handle2[2][1].y=1.2*100, handle2[2][1].z=-r;
	handle2[2][2].x=-2.65*100, handle2[2][2].y=0.7875*100, handle2[2][2].z=-r;
	handle2[2][3].x=-1.9*100, handle2[2][3].y=0.45*100, handle2[2][3].z=-r;

	handle2[3][0].x=-3.0*100, handle2[3][0].y=1.65*100, handle2[3][0].z=0.0;
	handle2[3][1].x=-3.0*100, handle2[3][1].y=1.2*100, handle2[3][1].z=0.0;
	handle2[3][2].x=-2.65*100, handle2[3][2].y=0.7875*100, handle2[3][2].z=0.0;
	handle2[3][3].x=-1.9*100, handle2[3][3].y=0.45*100, handle2[3][3].z=0.0;
}


void UtahTeapot::ReadMouseControlPoint(void)
{
	double r=0.1125;
	//��ֵ����첿��Ƭ1
	mouse[0][0].x=1.7*100, mouse[0][0].y=1.275*100, mouse[0][0].z=0.0;
	mouse[0][1].x=2.6*100, mouse[0][1].y=1.275*100, mouse[0][1].z=0.0;
	mouse[0][2].x=2.3*100, mouse[0][2].y=1.95*100, mouse[0][2].z=0.0;
	mouse[0][3].x=2.7*100, mouse[0][3].y=2.25*100, mouse[0][3].z=0.0;

	mouse[1][0].x=1.7*100, mouse[1][0].y=1.275*100, mouse[1][0].z=r;
	mouse[1][1].x=2.6*100, mouse[1][1].y=1.275*100, mouse[1][1].z=r;
	mouse[1][2].x=2.3*100, mouse[1][2].y=1.95*100, mouse[1][2].z=r;
	mouse[1][3].x=2.7*100, mouse[1][3].y=2.25*100, mouse[1][3].z=r;

	mouse[2][0].x=1.7*100, mouse[2][0].y=0.45*100, mouse[2][0].z=r;
	mouse[2][1].x=3.1*100, mouse[2][1].y=0.675*100, mouse[2][1].z=r;
	mouse[2][2].x=2.4*100, mouse[2][2].y=1.875*100, mouse[2][2].z=r;
	mouse[2][3].x=3.3*100, mouse[2][3].y=2.25*100, mouse[2][3].z=r;

	mouse[3][0].x=1.7*100, mouse[3][0].y=0.45*100, mouse[3][0].z=0.0;
	mouse[3][1].x=3.1*100, mouse[3][1].y=0.675*100, mouse[3][1].z=0.0;
	mouse[3][2].x=2.4*100, mouse[3][2].y=1.875*100, mouse[3][2].z=0.0;
	mouse[3][3].x=3.3*100, mouse[3][3].y=2.25*100, mouse[3][3].z=0.0;

	//��ֵ����첿��Ƭ2
	mouse2[0][0].x=2.7*100, mouse2[0][0].y=2.25*100, mouse2[0][0].z=0.0;
	mouse2[0][1].x=2.8*100, mouse2[0][1].y=2.325*100, mouse2[0][1].z=0.0;
	mouse2[0][2].x=2.9*100, mouse2[0][2].y=2.325*100, mouse2[0][2].z=0.0;
	mouse2[0][3].x=2.8*100, mouse2[0][3].y=2.25*100, mouse2[0][3].z=0.0;

	mouse2[1][0].x=2.7*100, mouse2[1][0].y=2.25*100, mouse2[1][0].z=r;
	mouse2[1][1].x=2.8*100, mouse2[1][1].y=2.325*100, mouse2[1][1].z=r;
	mouse2[1][2].x=2.9*100, mouse2[1][2].y=2.325*100, mouse2[1][2].z=r;
	mouse2[1][3].x=2.8*100, mouse2[1][3].y=2.25*100, mouse2[1][3].z=r;

	mouse2[2][0].x=3.3*100, mouse2[2][0].y=2.25*100, mouse2[2][0].z=r;
	mouse2[2][1].x=3.525*100, mouse2[2][1].y=2.34375*100, mouse2[2][1].z=r;
	mouse2[2][2].x=3.45*100, mouse2[2][2].y=2.3625*100, mouse2[2][2].z=r;
	mouse2[2][3].x=3.2*100, mouse2[2][3].y=2.25*100, mouse2[2][3].z=r;

	mouse2[3][0].x=3.3*100, mouse2[3][0].y=2.25*100, mouse2[3][0].z=0.0;
	mouse2[3][1].x=3.525*100, mouse2[3][1].y=2.34375*100, mouse2[3][1].z=0.0;
	mouse2[3][2].x=3.45*100, mouse2[3][2].y=2.3625*100, mouse2[3][2].z=0.0;
	mouse2[3][3].x=3.2*100, mouse2[3][3].y=2.25*100, mouse2[3][3].z=0.0;

	//��ֵ����첿��Ƭ3
	mouse3[0][0].x=1.7*100, mouse3[0][0].y=1.275*100, mouse3[0][0].z=0.0;
	mouse3[0][1].x=2.6*100, mouse3[0][1].y=1.275*100, mouse3[0][1].z=0.0;
	mouse3[0][2].x=2.3*100, mouse3[0][2].y=1.95*100, mouse3[0][2].z=0.0;
	mouse3[0][3].x=2.7*100, mouse3[0][3].y=2.25*100, mouse3[0][3].z=0.0;

	mouse3[1][0].x=1.7*100, mouse3[1][0].y=1.275*100, mouse3[1][0].z=-r;
	mouse3[1][1].x=2.6*100, mouse3[1][1].y=1.275*100, mouse3[1][1].z=-r;
	mouse3[1][2].x=2.3*100, mouse3[1][2].y=1.95*100, mouse3[1][2].z=-r;
	mouse3[1][3].x=2.7*100, mouse3[1][3].y=2.25*100, mouse3[1][3].z=-r;

	mouse3[2][0].x=1.7*100, mouse3[2][0].y=0.45*100, mouse3[2][0].z=-r;
	mouse3[2][1].x=3.1*100, mouse3[2][1].y=0.675*100, mouse3[2][1].z=-r;
	mouse3[2][2].x=2.4*100, mouse3[2][2].y=1.875*100, mouse3[2][2].z=-r;
	mouse3[2][3].x=3.3*100, mouse3[2][3].y=2.25*100, mouse3[2][3].z=-r;

	mouse3[3][0].x=1.7*100, mouse3[3][0].y=0.45*100, mouse3[3][0].z=0.0;
	mouse3[3][1].x=3.1*100, mouse3[3][1].y=0.675*100, mouse3[3][1].z=0.0;
	mouse3[3][2].x=2.4*100, mouse3[3][2].y=1.875*100, mouse3[3][2].z=0.0;
	mouse3[3][3].x=3.3*100, mouse3[3][3].y=2.25*100, mouse3[3][3].z=0.0;

	//��ֵ����첿��Ƭ4
	mouse4[0][0].x=2.7*100, mouse4[0][0].y=2.25*100, mouse4[0][0].z=0.0;
	mouse4[0][1].x=2.8*100, mouse4[0][1].y=2.325*100, mouse4[0][1].z=0.0;
	mouse4[0][2].x=2.9*100, mouse4[0][2].y=2.325*100, mouse4[0][2].z=0.0;
	mouse4[0][3].x=2.8*100, mouse4[0][3].y=2.25*100, mouse4[0][3].z=0.0;

	mouse4[1][0].x=2.7*100, mouse4[1][0].y=2.25*100, mouse4[1][0].z=-r;
	mouse4[1][1].x=2.8*100, mouse4[1][1].y=2.325*100, mouse4[1][1].z=-r;
	mouse4[1][2].x=2.9*100, mouse4[1][2].y=2.325*100, mouse4[1][2].z=-r;
	mouse4[1][3].x=2.8*100, mouse4[1][3].y=2.25*100, mouse4[1][3].z=-r;

	mouse4[2][0].x=3.3*100, mouse4[2][0].y=2.25*100, mouse4[2][0].z=-r;
	mouse4[2][1].x=3.525*100, mouse4[2][1].y=2.34375*100, mouse4[2][1].z=-r;
	mouse4[2][2].x=3.45*100, mouse4[2][2].y=2.3625*100, mouse4[2][2].z=-r;
	mouse4[2][3].x=3.2*100, mouse4[2][3].y=2.25*100, mouse4[2][3].z=-r;

	mouse4[3][0].x=3.3*100, mouse4[3][0].y=2.25*100, mouse4[3][0].z=0.0;
	mouse4[3][1].x=3.525*100, mouse4[3][1].y=2.34375*100, mouse4[3][1].z=0.0;
	mouse4[3][2].x=3.45*100, mouse4[3][2].y=2.3625*100, mouse4[3][2].z=0.0;
	mouse4[3][3].x=3.2*100, mouse4[3][3].y=2.25*100, mouse4[3][3].z=0.0;
}
