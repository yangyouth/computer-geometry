// BicubicBezierPatch.cpp: implementation of the CBicubicBezierPatch class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Test.h"
#include "BicubicBezierPatch.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBicubicBezierPatch::CBicubicBezierPatch(void)
{
}


CBicubicBezierPatch::~CBicubicBezierPatch(void)
{
}


void CBicubicBezierPatch::ReadControlPoint(CP3 P[4][4])
{
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
		{
			this->P[i][j]=P[i][j];
		}
}


void CBicubicBezierPatch::DrawCurvedPatch(CDC *pDC)
{
	double M[4][4];//系数矩阵
	M[0][0]=-1,M[0][1]=3,M[0][2]=-3,M[0][3]=1;
	M[1][0]=3,M[1][1]=-6,M[1][2]=3,M[1][3]=0;
	M[2][0]=-3,M[2][1]=3,M[2][2]=0,M[2][3]=0;
	M[3][0]=1,M[3][1]=0,M[3][2]=0,M[3][3]=0;

	LeftMultiplyMatrix(M,P);//控制点矩阵左乘系数矩阵
	RightMultiplyMatrix(P,M);//右乘系数矩阵
	double tStep=0.1;//步长
	double u0,u1,u2,u3,v0,v1,v2,v3;//u,v的参数
	for(double u=0;u<=1.0;u=u+tStep)
	{
		for(double v=0;v<=1.0;v=v+tStep)
		{
			u3=u*u*u,u2=u*u,u1=u,u0=1;
			v3=v*v*v,v2=v*v,v1=v,v0=1;
			CP3 pt=(u3*P[0][0]+u2*P[1][0]+u1*P[2][0]+u0*P[3][0])*v3
				+(u3*P[0][1]+u2*P[1][1]+u1*P[2][1]+u0*P[3][1])*v2
				+(u3*P[0][2]+u2*P[1][2]+u1*P[2][2]+u0*P[3][2])*v1
				+(u3*P[0][3]+u2*P[1][3]+u1*P[2][3]+u0*P[3][3])*v0;
			CP2 Point2=ObliqueProjection(pt);//斜二测投影
			if(v==0)
				pDC->MoveTo(ROUND(Point2.x),ROUND(Point2.y));
			else
				pDC->LineTo(ROUND(Point2.x),ROUND(Point2.y));
		}
	}
		
	for(double v=0;v<=1.0;v=v+tStep)
	{
		for(double u=0;u<=1.0;u=u+tStep)
		{
			u3=u*u*u,u2=u*u,u1=u,u0=1;
			v3=v*v*v,v2=v*v,v1=v,v0=1;
			CP3 pt=(u3*P[0][0]+u2*P[1][0]+u1*P[2][0]+u0*P[3][0])*v3
				+(u3*P[0][1]+u2*P[1][1]+u1*P[2][1]+u0*P[3][1])*v2
				+(u3*P[0][2]+u2*P[1][2]+u1*P[2][2]+u0*P[3][2])*v1
				+(u3*P[0][3]+u2*P[1][3]+u1*P[2][3]+u0*P[3][3])*v0;
			CP2 Point2=ObliqueProjection(pt);//斜二测投影->直角投影
			if(u==0)
				pDC->MoveTo(ROUND(Point2.x),ROUND(Point2.y));
			else
				pDC->LineTo(ROUND(Point2.x),ROUND(Point2.y));
		}
	}
}


void CBicubicBezierPatch::LeftMultiplyMatrix(double M[][4],CP3 P[][4])
{
	CP3 T[4][4];
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			T[i][j]=M[i][0]*P[0][j]+M[i][1]*P[1][j]+M[i][2]*P[2][j]+M[i][3]*P[3][j];
		}
	}
	for(i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			P[i][j]=T[i][j];
		}
	}
}


void CBicubicBezierPatch::RightMultiplyMatrix(CP3 P[][4],double M[][4])
{
	CP3 T[4][4];
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			T[i][j]=P[i][0]*M[0][j]+P[i][1]*M[1][j]+P[i][2]*M[2][j]+P[i][3]*M[3][j];
		}
	}
	for(i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			P[i][j]=T[i][j];
		}
	}
}


void CBicubicBezierPatch::TransposeMatrix(double M[][4])
{
	double T[4][4];
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			T[i][j]=M[j][i];
		}
	}
	for(i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			M[i][j]=T[i][j];
		}
	}
}


CP2 CBicubicBezierPatch::ObliqueProjection(CP3 Point3)
{
	CP2 Point2;
	Point2.x=Point3.x;
	Point2.y=Point3.y;
	return Point2;
}

void CBicubicBezierPatch::DrawControlGrid(CDC *pDC)
{
	CP2 P2[4][4];
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			P2[i][j]=ObliqueProjection(P[i][j]);
		}
	}
	CPen NewPen,*pOldPen;
	NewPen.CreatePen(PS_SOLID,3,RGB(0,0,0));
	pOldPen=pDC->SelectObject(&NewPen);
	for(i=0;i<4;i++)
	{
		pDC->MoveTo(ROUND(P2[i][0].x),ROUND(P2[i][0].y));
		for(int j=0;j<4;j++)
		{
			pDC->LineTo(ROUND(P2[i][j].x),ROUND(P2[i][j].y));
		}
	}

	for(i=0;i<4;i++)
	{
		pDC->MoveTo(ROUND(P2[0][i].x),ROUND(P2[0][i].y));
		for(int j=0;j<4;j++)
		{
			pDC->LineTo(ROUND(P2[j][i].x),ROUND(P2[j][i].y));
		}
	}
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
}
