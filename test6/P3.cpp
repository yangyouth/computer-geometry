// P3.cpp: implementation of the CP3 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Test.h"
#include "P3.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CP3::CP3(void)
{
		w=1;//做仿射变换时使用
}


CP3::~CP3(void)
{
}

CP3::CP3(double x,double y,double z)
{
	this->x=x;
	this->y=y;
	this->z=z;
}

CP3 operator*(double scale,const CP3&P)
{
	CP3 tempP3;
	tempP3.x=scale*P.x;
	tempP3.y=scale*P.y;
	tempP3.z=scale*P.z;
	return tempP3;
}

CP3 operator+(const CP3&P1,const CP3&P2)
{
	CP3 tempP3;
	tempP3.x=P1.x+P2.x;
	tempP3.y=P1.y+P2.y;
	tempP3.z=P1.z+P2.z;
	return tempP3;
}

CP3 operator*(const CP3&P,double scale)
{
	CP3 tempP3;
	tempP3.x=scale*P.x;
	tempP3.y=scale*P.y;
	tempP3.z=scale*P.z;
	return tempP3;
}
