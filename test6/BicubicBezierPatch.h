// BicubicBezierPatch.h: interface for the CBicubicBezierPatch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BICUBICBEZIERPATCH_H__4158AD1A_004F_4036_AE39_4A0688C4C933__INCLUDED_)
#define AFX_BICUBICBEZIERPATCH_H__4158AD1A_004F_4036_AE39_4A0688C4C933__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include"P3.h"
#include"P2.h"
#include"math.h"
#define ROUND(h) int((h)+0.5)


class CBicubicBezierPatch
{
public:
	CBicubicBezierPatch(void);
	~CBicubicBezierPatch(void);
public:
	void ReadControlPoint(CP3 P[4][4]);//读取16个控制点
	void DrawCurvedPatch(CDC *pDC);//绘制双三次Bezier曲面片
	void DrawControlGrid(CDC *pDC);//绘制控制网格
private:
	void LeftMultiplyMatrix(double M[][4],CP3 P[][4]);//左乘顶点矩阵
	void RightMultiplyMatrix(CP3 P[][4],double M[][4]);//右乘的顶点矩阵
	void TransposeMatrix(double M[][4]);//转置矩阵
	CP2 ObliqueProjection(CP3 Point3);//斜二测投影显示->直角投影
public:
	CP3 P[4][4];//三维控制点
};

#endif // !defined(AFX_BICUBICBEZIERPATCH_H__4158AD1A_004F_4036_AE39_4A0688C4C933__INCLUDED_)
