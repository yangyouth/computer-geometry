// UtahTeapot.h: interface for the UtahTeapot class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTAHTEAPOT_H__3AACCB98_A165_4B1F_8F55_3C65C8A75418__INCLUDED_)
#define AFX_UTAHTEAPOT_H__3AACCB98_A165_4B1F_8F55_3C65C8A75418__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#include "P3.h"
#include"Revolution.h"
#include"BicubicBezierPatch.h"
#endif // _MSC_VER > 1000

class UtahTeapot//绘制犹他茶壶
{
public:
	UtahTeapot(void);
	~UtahTeapot(void);

private:
	CP3 bodyPoint[3][4];//茶壶体部分由三段四次Bezier曲线旋转而成
	CP3 gaiPoint[2][4];//壶盖部分两段三次Bezier曲线旋转构成
	CP3 bottom[1][4];//茶壶底部由一条Bezier曲线旋转构成
	CP3 P3[4][4];//曲面控制点

	CP3 handle[4][4];//茶壶柄部点表
	CP3 handle2[4][4];//茶壶柄部点表
	CP3 handle3[4][4];//茶壶柄部点表
	CP3 handle4[4][4];//茶壶柄部点表

	CP3 mouse[4][4];//茶壶嘴部点表
	CP3 mouse2[4][4];//茶壶嘴部点表
	CP3 mouse3[4][4];//茶壶嘴部点表
	CP3 mouse4[4][4];//茶壶嘴部点表
public:
	void DrawTeapot(CDC*pDC);
	CRevolution draw;//绘制旋转体
	CBicubicBezierPatch drawBzeierPatch;//绘制拼接面

	void ReadHandleControlPoint(void);//手柄部分控制点
	void ReadMouseControlPoint(void);//嘴部控制点
	void ReadPoint(void);//其余控制点读取
};

#endif // !defined(AFX_UTAHTEAPOT_H__3AACCB98_A165_4B1F_8F55_3C65C8A75418__INCLUDED_)
