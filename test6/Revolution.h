// Revolution.h: interface for the CRevolution class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVOLUTION_H__84B7A85F_85EE_4534_9496_6AE736DFA8C4__INCLUDED_)
#define AFX_REVOLUTION_H__84B7A85F_85EE_4534_9496_6AE736DFA8C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include"P2.h"
#include"P3.h"
#include"Patch.h"
#include"BicubicBezierPatch.h"
#include"math.h"
#define ROUND(h) int((h)+0.5)

class CRevolution//旋转体类
{
public:
	CRevolution(void);
	~CRevolution(void);
public:
	void ReadCubicBezierControlPoint(CP3*ctrP);//读入四个二维控制点
	void ReadVertex(void);//旋转体控制多边形顶点
	void ReadPatch(void);//读入旋转体双三次曲面片
	void DrawRevolutionSurface(CDC*pDC);//绘制旋转体曲面
public:
	CP3 V[64];//旋转曲面总定点数
private:
	CP3 P[4];//曲线的四个三维控制点
	CPatch Patch[4];//旋转曲面面片数
	CP3 P3[4][4];//每个面片的控制点数
	CBicubicBezierPatch surf;//实例化对象绘制
};

#endif // !defined(AFX_REVOLUTION_H__84B7A85F_85EE_4534_9496_6AE736DFA8C4__INCLUDED_)
